Sistema de Depósito

Este é um sistema de depósito desenvolvido em Java onde os usuários podem fazer login e cadastrar produtos.

## Funcionalidades

- **Login**: Os usuários podem fazer login usando suas credenciais.
- **Cadastro de Produtos**: Os usuários podem cadastrar produtos no sistema, incluindo informações como nome, descrição e quantidade.
- **Visualização de Produtos**: Os usuários podem visualizar todos os produtos cadastrados.


## Instalação

1. Clone este repositório:

```bash
git clone git@gitlab.com:alves-py/lab7sd3.git
```

2. Importe o projeto para a sua IDE Java.

3. Configure as credenciais do banco de dados no arquivo `application.properties`.